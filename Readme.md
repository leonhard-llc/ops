# Ops Tools

- [dns-server/](dns-server/) - A threaded DNS server with no `unsafe`.

# Supporting Crates

- [any-range/](any-range/) - `AnyRange<T>` enum can hold any `Range*<T>` type
- [build-data/](build-data/) - Include build data in your program
- [build-data-test/](build-data-test/) - Integration test for `build-data` crate
- [dns-server/](dns-server/) - DNS server library
- [fair-rate-limiter/](fair-rate-limiter/) - Detect overload and fairly shed load from diverse IPs, subnets, users, or systems. Mitigate denial-of-service (DoS) attacks.
- [permit/](permit/) - A struct for cancelling operations
- [prob-rate-limiter/](prob-rate-limiter/) - Proababilistic rate limiter. Smoothly shed load to prevent overload.
- [rustls-pin/](rustls-pin/) - Server certificate pinning with `rustls`
- [temp-dir/](temp-dir/) - Simple temporary directory with cleanup
- [temp-file/](temp-file/) - Simple temporary file with cleanup
